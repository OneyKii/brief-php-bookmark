<?php

$host = 'mysql:host=localhost;dbname=bookmark_v3;charset=utf8';
$username = 'root';
$password = 'root';

try {
    $db = new PDO($host, $username, $password);
} catch (PDOException $e) {
    die('Erreur : ' . $e->getMessage());
}

